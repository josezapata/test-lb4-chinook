import { Entity, model, property, hasMany } from '@loopback/repository';
import { Invoiceline } from './invoiceline.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'Invoice' } }
})
export class Invoice extends Entity {
  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 1,
    postgresql: { columnName: 'invoiceid', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  invoiceid: number;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: { columnName: 'CustomerId', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  customerid: number;

  @property({
    type: 'date',
    required: true,
    postgresql: { columnName: 'InvoiceDate', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  invoicedate: string;

  @property({
    type: 'string',
    length: 70,
    postgresql: { columnName: 'BillingAddress', dataType: 'character varying', dataLength: 70, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  billingaddress?: string;

  @property({
    type: 'string',
    length: 40,
    postgresql: { columnName: 'BillingCity', dataType: 'character varying', dataLength: 40, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  billingcity?: string;

  @property({
    type: 'string',
    length: 40,
    postgresql: { columnName: 'BillingState', dataType: 'character varying', dataLength: 40, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  billingstate?: string;

  @property({
    type: 'string',
    length: 40,
    postgresql: { columnName: 'BillingCountry', dataType: 'character varying', dataLength: 40, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  billingcountry?: string;

  @property({
    type: 'string',
    length: 10,
    postgresql: { columnName: 'BillingPostalCode', dataType: 'character varying', dataLength: 10, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  billingpostalcode?: string;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 2,
    postgresql: { columnName: 'Total', dataType: 'numeric', dataLength: null, dataPrecision: 10, dataScale: 2, nullable: 'NO' },
  })
  total: number;

  @hasMany(() => Invoiceline, { keyTo: 'invoiceid' })
  invoicelines: Invoiceline[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Invoice>) {
    super(data);
  }
}

export interface InvoiceRelations {
  // describe navigational properties here
}

export type InvoiceWithRelations = Invoice & InvoiceRelations;
