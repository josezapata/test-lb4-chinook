import { Entity, model, property, hasMany } from '@loopback/repository';
import { Playlisttrack } from './playlisttrack.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'Playlist' } }
})
export class Playlist extends Entity {
  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 1,
    postgresql: { columnName: 'playlistid', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  playlistid: number;

  @property({
    type: 'string',
    length: 120,
    postgresql: { columnName: 'Name', dataType: 'character varying', dataLength: 120, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  name?: string;

  @hasMany(() => Playlisttrack, { keyTo: 'playlistid' })
  playlisttracks: Playlisttrack[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Playlist>) {
    super(data);
  }
}

export interface PlaylistRelations {
  // describe navigational properties here
}

export type PlaylistWithRelations = Playlist & PlaylistRelations;
