import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Playlist } from "./playlist.model";
import { Track } from "./track.model";

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'PlaylistTrack' } }
})
export class Playlisttrack extends Entity {
  @belongsTo(() => Playlist, { keyFrom: 'playlistid', name: 'playlist' }, { name: 'playlistid', id: 1 })
  playlistid: number;

  @belongsTo(() => Track, { keyFrom: 'trackid', name: 'track' }, { name: 'trackid' })
  trackid: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Playlisttrack>) {
    super(data);
  }
}

export interface PlaylisttrackRelations {
  // describe navigational properties here
}

export type PlaylisttrackWithRelations = Playlisttrack & PlaylisttrackRelations;
