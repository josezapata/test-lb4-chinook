import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'public', table: 'MediaType'}}
})
export class Mediatype extends Entity {
  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 1,
    postgresql: {columnName: 'MediaTypeId', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  mediatypeid: number;

  @property({
    type: 'string',
    length: 120,
    postgresql: {columnName: 'Name', dataType: 'character varying', dataLength: 120, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  name?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Mediatype>) {
    super(data);
  }
}

export interface MediatypeRelations {
  // describe navigational properties here
}

export type MediatypeWithRelations = Mediatype & MediatypeRelations;
