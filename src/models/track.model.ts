import { Entity, model, property, hasMany } from '@loopback/repository';
import { Playlisttrack } from './playlisttrack.model';
import { Invoiceline } from './invoiceline.model';

@model({ settings: { idInjection: false, postgresql: { schema: 'public', table: 'Track' } } })
export class Track extends Entity {
  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 1,
    postgresql: { columnName: 'trackid', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  trackid: number;

  @property({
    type: 'string',
    required: true,
    length: 200,
    postgresql: { columnName: 'Name', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  name: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: { columnName: 'AlbumId', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES' },
  })
  albumid?: number;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: { columnName: 'MediaTypeId', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  mediatypeid: number;

  @property({
    type: 'number',
    scale: 0,
    postgresql: { columnName: 'GenreId', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES' },
  })
  genreid?: number;

  @property({
    type: 'string',
    length: 220,
    postgresql: { columnName: 'Composer', dataType: 'character varying', dataLength: 220, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  composer?: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: { columnName: 'Milliseconds', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  milliseconds: number;

  @property({
    type: 'number',
    scale: 0,
    postgresql: { columnName: 'Bytes', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES' },
  })
  bytes?: number;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 2,
    postgresql: { columnName: 'UnitPrice', dataType: 'numeric', dataLength: null, dataPrecision: 10, dataScale: 2, nullable: 'NO' },
  })
  unitprice: number;

  @hasMany(() => Playlisttrack, { keyTo: 'trackid' })
  playlisttracks: Playlisttrack[];

  @hasMany(() => Invoiceline, { keyTo: 'trackid' })
  invoicelines: Invoiceline[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Track>) {
    super(data);
  }
}

export interface TrackRelations {
  // describe navigational properties here
}

export type TrackWithRelations = Track & TrackRelations;
