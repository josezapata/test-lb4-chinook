import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Track } from './track.model';
import { Invoice } from './invoice.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'InvoiceLine' } }
})
export class Invoiceline extends Entity {
  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 1,
    postgresql: { columnName: 'invoicelineid', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  invoicelineid: number;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 2,
    postgresql: { columnName: 'UnitPrice', dataType: 'numeric', dataLength: null, dataPrecision: 10, dataScale: 2, nullable: 'NO' },
  })
  unitprice: number;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: { columnName: 'Quantity', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO' },
  })
  quantity: number;

  @belongsTo(() => Track, { keyTo: 'trackid' }, { name: 'trackid' })
  trackId: number;

  @belongsTo(() => Invoice, { keyTo: 'invoiceid' }, { name: 'invoiceid' })
  invoiceId: number;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Invoiceline>) {
    super(data);
  }
}

export interface InvoicelineRelations {
  // describe navigational properties here
}

export type InvoicelineWithRelations = Invoiceline & InvoicelineRelations;
