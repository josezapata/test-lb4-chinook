import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'public', table: 'Employee'}}
})
export class Employee extends Entity {
  @property({
    type: 'number',
    required: true,
    scale: 0,
    id: 1,
    postgresql: {columnName: 'EmployeeId', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  employeeid: number;

  @property({
    type: 'string',
    required: true,
    length: 20,
    postgresql: {columnName: 'LastName', dataType: 'character varying', dataLength: 20, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  lastname: string;

  @property({
    type: 'string',
    required: true,
    length: 20,
    postgresql: {columnName: 'FirstName', dataType: 'character varying', dataLength: 20, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  firstname: string;

  @property({
    type: 'string',
    length: 30,
    postgresql: {columnName: 'Title', dataType: 'character varying', dataLength: 30, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  title?: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'ReportsTo', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  reportsto?: number;

  @property({
    type: 'date',
    postgresql: {columnName: 'BirthDate', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  birthdate?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'HireDate', dataType: 'timestamp without time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  hiredate?: string;

  @property({
    type: 'string',
    length: 70,
    postgresql: {columnName: 'Address', dataType: 'character varying', dataLength: 70, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  address?: string;

  @property({
    type: 'string',
    length: 40,
    postgresql: {columnName: 'City', dataType: 'character varying', dataLength: 40, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  city?: string;

  @property({
    type: 'string',
    length: 40,
    postgresql: {columnName: 'State', dataType: 'character varying', dataLength: 40, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  state?: string;

  @property({
    type: 'string',
    length: 40,
    postgresql: {columnName: 'Country', dataType: 'character varying', dataLength: 40, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  country?: string;

  @property({
    type: 'string',
    length: 10,
    postgresql: {columnName: 'PostalCode', dataType: 'character varying', dataLength: 10, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  postalcode?: string;

  @property({
    type: 'string',
    length: 24,
    postgresql: {columnName: 'Phone', dataType: 'character varying', dataLength: 24, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  phone?: string;

  @property({
    type: 'string',
    length: 24,
    postgresql: {columnName: 'Fax', dataType: 'character varying', dataLength: 24, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  fax?: string;

  @property({
    type: 'string',
    length: 60,
    postgresql: {columnName: 'Email', dataType: 'character varying', dataLength: 60, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  email?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Employee>) {
    super(data);
  }
}

export interface EmployeeRelations {
  // describe navigational properties here
}

export type EmployeeWithRelations = Employee & EmployeeRelations;
