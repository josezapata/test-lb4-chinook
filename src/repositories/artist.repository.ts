import {DefaultCrudRepository} from '@loopback/repository';
import {Artist, ArtistRelations} from '../models';
import {PgdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ArtistRepository extends DefaultCrudRepository<
  Artist,
  typeof Artist.prototype.artistid,
  ArtistRelations
> {
  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource,
  ) {
    super(Artist, dataSource);
  }
}
