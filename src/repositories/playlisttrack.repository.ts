import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Playlisttrack, PlaylisttrackRelations, Track, Playlist} from '../models';
import {PgdbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {TrackRepository} from './track.repository';
import {PlaylistRepository} from './playlist.repository';

export class PlaylisttrackRepository extends DefaultCrudRepository<
  Playlisttrack,
  typeof Playlisttrack.prototype.id,
  PlaylisttrackRelations
> {

  public readonly track: BelongsToAccessor<Track, typeof Playlisttrack.prototype.null>;

  public readonly playlist: BelongsToAccessor<Playlist, typeof Playlisttrack.prototype.null>;

  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource, @repository.getter('TrackRepository') protected trackRepositoryGetter: Getter<TrackRepository>, @repository.getter('PlaylistRepository') protected playlistRepositoryGetter: Getter<PlaylistRepository>,
  ) {
    super(Playlisttrack, dataSource);
    this.playlist = this.createBelongsToAccessorFor('playlist', playlistRepositoryGetter,);
    this.registerInclusionResolver('playlist', this.playlist.inclusionResolver);
    this.track = this.createBelongsToAccessorFor('track', trackRepositoryGetter,);
    this.registerInclusionResolver('track', this.track.inclusionResolver);
  }
}
