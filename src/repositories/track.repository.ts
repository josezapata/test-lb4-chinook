import { DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import { Track, TrackRelations, Playlisttrack, Invoiceline} from '../models';
import { PgdbDataSource } from '../datasources';
import { inject, Getter} from '@loopback/core';
import {PlaylisttrackRepository} from './playlisttrack.repository';
import {InvoicelineRepository} from './invoiceline.repository';

export class TrackRepository extends DefaultCrudRepository<
  Track,
  typeof Track.prototype.trackid,
  TrackRelations
  > {

  public readonly playlisttracks: HasManyRepositoryFactory<Playlisttrack, typeof Track.prototype.trackid>;

  public readonly invoicelines: HasManyRepositoryFactory<Invoiceline, typeof Track.prototype.trackid>;

  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource, @repository.getter('PlaylisttrackRepository') protected playlisttrackRepositoryGetter: Getter<PlaylisttrackRepository>, @repository.getter('InvoicelineRepository') protected invoicelineRepositoryGetter: Getter<InvoicelineRepository>,
  ) {
    super(Track, dataSource);
    this.invoicelines = this.createHasManyRepositoryFactoryFor('invoicelines', invoicelineRepositoryGetter,);
    this.registerInclusionResolver('invoicelines', this.invoicelines.inclusionResolver);
    this.playlisttracks = this.createHasManyRepositoryFactoryFor('playlisttracks', playlisttrackRepositoryGetter,);
    this.registerInclusionResolver('playlisttracks', this.playlisttracks.inclusionResolver);
  }
}
