import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Invoiceline, InvoicelineRelations, Track, Invoice} from '../models';
import {PgdbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {TrackRepository} from './track.repository';
import {InvoiceRepository} from './invoice.repository';

export class InvoicelineRepository extends DefaultCrudRepository<
  Invoiceline,
  typeof Invoiceline.prototype.invoicelineid,
  InvoicelineRelations
> {

  public readonly track: BelongsToAccessor<Track, typeof Invoiceline.prototype.invoicelineid>;

  public readonly invoice: BelongsToAccessor<Invoice, typeof Invoiceline.prototype.invoicelineid>;

  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource, @repository.getter('TrackRepository') protected trackRepositoryGetter: Getter<TrackRepository>, @repository.getter('InvoiceRepository') protected invoiceRepositoryGetter: Getter<InvoiceRepository>,
  ) {
    super(Invoiceline, dataSource);
    this.invoice = this.createBelongsToAccessorFor('invoice', invoiceRepositoryGetter,);
    this.registerInclusionResolver('invoice', this.invoice.inclusionResolver);
    this.track = this.createBelongsToAccessorFor('track', trackRepositoryGetter,);
    this.registerInclusionResolver('track', this.track.inclusionResolver);
  }
}
