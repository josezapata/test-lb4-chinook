import {DefaultCrudRepository} from '@loopback/repository';
import {Album, AlbumRelations} from '../models';
import {PgdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AlbumRepository extends DefaultCrudRepository<
  Album,
  typeof Album.prototype.albumid,
  AlbumRelations
> {
  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource,
  ) {
    super(Album, dataSource);
  }
}
