import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Invoice, InvoiceRelations, Invoiceline} from '../models';
import {PgdbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {InvoicelineRepository} from './invoiceline.repository';

export class InvoiceRepository extends DefaultCrudRepository<
  Invoice,
  typeof Invoice.prototype.invoiceid,
  InvoiceRelations
> {

  public readonly invoicelines: HasManyRepositoryFactory<Invoiceline, typeof Invoice.prototype.invoiceid>;

  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource, @repository.getter('InvoicelineRepository') protected invoicelineRepositoryGetter: Getter<InvoicelineRepository>,
  ) {
    super(Invoice, dataSource);
    this.invoicelines = this.createHasManyRepositoryFactoryFor('invoicelines', invoicelineRepositoryGetter,);
    this.registerInclusionResolver('invoicelines', this.invoicelines.inclusionResolver);
  }
}
