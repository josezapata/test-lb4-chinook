import { DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import { Playlist, PlaylistRelations, Playlisttrack} from '../models';
import { PgdbDataSource } from '../datasources';
import { inject, Getter} from '@loopback/core';
import {PlaylisttrackRepository} from './playlisttrack.repository';

export class PlaylistRepository extends DefaultCrudRepository<
  Playlist,
  typeof Playlist.prototype.playlistid,
  PlaylistRelations
  > {

  public readonly playlisttracks: HasManyRepositoryFactory<Playlisttrack, typeof Playlist.prototype.playlistid>;

  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource, @repository.getter('PlaylisttrackRepository') protected playlisttrackRepositoryGetter: Getter<PlaylisttrackRepository>,
  ) {
    super(Playlist, dataSource);
    this.playlisttracks = this.createHasManyRepositoryFactoryFor('playlisttracks', playlisttrackRepositoryGetter,);
    this.registerInclusionResolver('playlisttracks', this.playlisttracks.inclusionResolver);
  }
}
