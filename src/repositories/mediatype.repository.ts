import {DefaultCrudRepository} from '@loopback/repository';
import {Mediatype, MediatypeRelations} from '../models';
import {PgdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class MediatypeRepository extends DefaultCrudRepository<
  Mediatype,
  typeof Mediatype.prototype.mediatypeid,
  MediatypeRelations
> {
  constructor(
    @inject('datasources.pgdb') dataSource: PgdbDataSource,
  ) {
    super(Mediatype, dataSource);
  }
}
