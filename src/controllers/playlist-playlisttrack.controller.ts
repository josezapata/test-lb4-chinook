import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Playlist,
  Playlisttrack,
} from '../models';
import {PlaylistRepository} from '../repositories';

export class PlaylistPlaylisttrackController {
  constructor(
    @repository(PlaylistRepository) protected playlistRepository: PlaylistRepository,
  ) { }

  @get('/playlists/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Array of Playlisttrack\'s belonging to Playlist',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Playlisttrack)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Playlisttrack>,
  ): Promise<Playlisttrack[]> {
    return this.playlistRepository.playlisttracks(id).find(filter);
  }

  @post('/playlists/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Playlist model instance',
        content: {'application/json': {schema: getModelSchemaRef(Playlisttrack)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Playlist.prototype.playlistid,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Playlisttrack, {
            title: 'NewPlaylisttrackInPlaylist',
            exclude: [''],
            optional: ['playlistId']
          }),
        },
      },
    }) playlisttrack: Omit<Playlisttrack, ''>,
  ): Promise<Playlisttrack> {
    return this.playlistRepository.playlisttracks(id).create(playlisttrack);
  }

  @patch('/playlists/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Playlist.Playlisttrack PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Playlisttrack, {partial: true}),
        },
      },
    })
    playlisttrack: Partial<Playlisttrack>,
    @param.query.object('where', getWhereSchemaFor(Playlisttrack)) where?: Where<Playlisttrack>,
  ): Promise<Count> {
    return this.playlistRepository.playlisttracks(id).patch(playlisttrack, where);
  }

  @del('/playlists/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Playlist.Playlisttrack DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Playlisttrack)) where?: Where<Playlisttrack>,
  ): Promise<Count> {
    return this.playlistRepository.playlisttracks(id).delete(where);
  }
}
