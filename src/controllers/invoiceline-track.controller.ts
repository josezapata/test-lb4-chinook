import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Invoiceline,
  Track,
} from '../models';
import {InvoicelineRepository} from '../repositories';

export class InvoicelineTrackController {
  constructor(
    @repository(InvoicelineRepository)
    public invoicelineRepository: InvoicelineRepository,
  ) { }

  @get('/invoicelines/{id}/track', {
    responses: {
      '200': {
        description: 'Track belonging to Invoiceline',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Track)},
          },
        },
      },
    },
  })
  async getTrack(
    @param.path.number('id') id: typeof Invoiceline.prototype.invoicelineid,
  ): Promise<Track> {
    return this.invoicelineRepository.track(id);
  }
}
