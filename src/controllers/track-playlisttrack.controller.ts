import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Track,
  Playlisttrack,
} from '../models';
import {TrackRepository} from '../repositories';

export class TrackPlaylisttrackController {
  constructor(
    @repository(TrackRepository) protected trackRepository: TrackRepository,
  ) { }

  @get('/tracks/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Array of Playlisttrack\'s belonging to Track',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Playlisttrack)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Playlisttrack>,
  ): Promise<Playlisttrack[]> {
    return this.trackRepository.playlisttracks(id).find(filter);
  }

  @post('/tracks/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Track model instance',
        content: {'application/json': {schema: getModelSchemaRef(Playlisttrack)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Track.prototype.trackid,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Playlisttrack, {
            title: 'NewPlaylisttrackInTrack',
            exclude: [''],
            optional: ['trackId']
          }),
        },
      },
    }) playlisttrack: Omit<Playlisttrack, ''>,
  ): Promise<Playlisttrack> {
    return this.trackRepository.playlisttracks(id).create(playlisttrack);
  }

  @patch('/tracks/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Track.Playlisttrack PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Playlisttrack, {partial: true}),
        },
      },
    })
    playlisttrack: Partial<Playlisttrack>,
    @param.query.object('where', getWhereSchemaFor(Playlisttrack)) where?: Where<Playlisttrack>,
  ): Promise<Count> {
    return this.trackRepository.playlisttracks(id).patch(playlisttrack, where);
  }

  @del('/tracks/{id}/playlisttracks', {
    responses: {
      '200': {
        description: 'Track.Playlisttrack DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Playlisttrack)) where?: Where<Playlisttrack>,
  ): Promise<Count> {
    return this.trackRepository.playlisttracks(id).delete(where);
  }
}
