import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Playlisttrack,
  Track,
} from '../models';
import { PlaylisttrackRepository } from '../repositories';

export class PlaylisttrackTrackController {
  constructor(
    @repository(PlaylisttrackRepository)
    public playlisttrackRepository: PlaylisttrackRepository,
  ) { }

  @get('/playlisttracks/{id}/track', {
    responses: {
      '200': {
        description: 'Track belonging to Playlisttrack',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Track) },
          },
        },
      },
    },
  })
  async getTrack(
    @param.path.number('id') id: typeof Playlisttrack.prototype.trackId,
  ): Promise<Track> {
    return this.playlisttrackRepository.track(id);
  }
}
