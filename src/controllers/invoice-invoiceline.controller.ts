import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Invoice,
  Invoiceline,
} from '../models';
import {InvoiceRepository} from '../repositories';

export class InvoiceInvoicelineController {
  constructor(
    @repository(InvoiceRepository) protected invoiceRepository: InvoiceRepository,
  ) { }

  @get('/invoices/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Array of Invoiceline\'s belonging to Invoice',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Invoiceline)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Invoiceline>,
  ): Promise<Invoiceline[]> {
    return this.invoiceRepository.invoicelines(id).find(filter);
  }

  @post('/invoices/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Invoice model instance',
        content: {'application/json': {schema: getModelSchemaRef(Invoiceline)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Invoice.prototype.invoiceid,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invoiceline, {
            title: 'NewInvoicelineInInvoice',
            exclude: ['invoicelineid'],
            optional: ['invoiceId']
          }),
        },
      },
    }) invoiceline: Omit<Invoiceline, 'invoicelineid'>,
  ): Promise<Invoiceline> {
    return this.invoiceRepository.invoicelines(id).create(invoiceline);
  }

  @patch('/invoices/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Invoice.Invoiceline PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invoiceline, {partial: true}),
        },
      },
    })
    invoiceline: Partial<Invoiceline>,
    @param.query.object('where', getWhereSchemaFor(Invoiceline)) where?: Where<Invoiceline>,
  ): Promise<Count> {
    return this.invoiceRepository.invoicelines(id).patch(invoiceline, where);
  }

  @del('/invoices/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Invoice.Invoiceline DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Invoiceline)) where?: Where<Invoiceline>,
  ): Promise<Count> {
    return this.invoiceRepository.invoicelines(id).delete(where);
  }
}
