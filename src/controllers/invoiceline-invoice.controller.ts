import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Invoiceline,
  Invoice,
} from '../models';
import {InvoicelineRepository} from '../repositories';

export class InvoicelineInvoiceController {
  constructor(
    @repository(InvoicelineRepository)
    public invoicelineRepository: InvoicelineRepository,
  ) { }

  @get('/invoicelines/{id}/invoice', {
    responses: {
      '200': {
        description: 'Invoice belonging to Invoiceline',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Invoice)},
          },
        },
      },
    },
  })
  async getInvoice(
    @param.path.number('id') id: typeof Invoiceline.prototype.invoicelineid,
  ): Promise<Invoice> {
    return this.invoicelineRepository.invoice(id);
  }
}
