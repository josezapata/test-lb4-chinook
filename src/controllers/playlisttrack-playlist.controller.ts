import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Playlisttrack,
  Playlist,
} from '../models';
import { PlaylisttrackRepository } from '../repositories';

export class PlaylisttrackPlaylistController {
  constructor(
    @repository(PlaylisttrackRepository)
    public playlisttrackRepository: PlaylisttrackRepository,
  ) { }

  @get('/playlisttracks/{id}/playlist', {
    responses: {
      '200': {
        description: 'Playlist belonging to Playlisttrack',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Playlist) },
          },
        },
      },
    },
  })
  async getPlaylist(
    @param.path.number('id') id: typeof Playlisttrack.prototype.playlistId,
  ): Promise<Playlist> {
    return this.playlisttrackRepository.playlist(id);
  }
}
