import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Playlisttrack} from '../models';
import {PlaylisttrackRepository} from '../repositories';

export class PlaylisttrackController {
  constructor(
    @repository(PlaylisttrackRepository)
    public playlisttrackRepository : PlaylisttrackRepository,
  ) {}

  @post('/playlisttracks', {
    responses: {
      '200': {
        description: 'Playlisttrack model instance',
        content: {'application/json': {schema: getModelSchemaRef(Playlisttrack)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Playlisttrack, {
            title: 'NewPlaylisttrack',
            
          }),
        },
      },
    })
    playlisttrack: Playlisttrack,
  ): Promise<Playlisttrack> {
    return this.playlisttrackRepository.create(playlisttrack);
  }

  @get('/playlisttracks/count', {
    responses: {
      '200': {
        description: 'Playlisttrack model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Playlisttrack)) where?: Where<Playlisttrack>,
  ): Promise<Count> {
    return this.playlisttrackRepository.count(where);
  }

  @get('/playlisttracks', {
    responses: {
      '200': {
        description: 'Array of Playlisttrack model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Playlisttrack, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Playlisttrack)) filter?: Filter<Playlisttrack>,
  ): Promise<Playlisttrack[]> {
    return this.playlisttrackRepository.find(filter);
  }

  @patch('/playlisttracks', {
    responses: {
      '200': {
        description: 'Playlisttrack PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Playlisttrack, {partial: true}),
        },
      },
    })
    playlisttrack: Playlisttrack,
    @param.query.object('where', getWhereSchemaFor(Playlisttrack)) where?: Where<Playlisttrack>,
  ): Promise<Count> {
    return this.playlisttrackRepository.updateAll(playlisttrack, where);
  }

  @get('/playlisttracks/{id}', {
    responses: {
      '200': {
        description: 'Playlisttrack model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Playlisttrack, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.query.object('filter', getFilterSchemaFor(Playlisttrack)) filter?: Filter<Playlisttrack>
  ): Promise<Playlisttrack> {
    return this.playlisttrackRepository.findById(id, filter);
  }

  @patch('/playlisttracks/{id}', {
    responses: {
      '204': {
        description: 'Playlisttrack PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Playlisttrack, {partial: true}),
        },
      },
    })
    playlisttrack: Playlisttrack,
  ): Promise<void> {
    await this.playlisttrackRepository.updateById(id, playlisttrack);
  }

  @put('/playlisttracks/{id}', {
    responses: {
      '204': {
        description: 'Playlisttrack PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() playlisttrack: Playlisttrack,
  ): Promise<void> {
    await this.playlisttrackRepository.replaceById(id, playlisttrack);
  }

  @del('/playlisttracks/{id}', {
    responses: {
      '204': {
        description: 'Playlisttrack DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.playlisttrackRepository.deleteById(id);
  }
}
