import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Track,
  Invoiceline,
} from '../models';
import {TrackRepository} from '../repositories';

export class TrackInvoicelineController {
  constructor(
    @repository(TrackRepository) protected trackRepository: TrackRepository,
  ) { }

  @get('/tracks/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Array of Invoiceline\'s belonging to Track',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Invoiceline)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Invoiceline>,
  ): Promise<Invoiceline[]> {
    return this.trackRepository.invoicelines(id).find(filter);
  }

  @post('/tracks/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Track model instance',
        content: {'application/json': {schema: getModelSchemaRef(Invoiceline)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Track.prototype.trackid,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invoiceline, {
            title: 'NewInvoicelineInTrack',
            exclude: ['invoicelineid'],
            optional: ['trackId']
          }),
        },
      },
    }) invoiceline: Omit<Invoiceline, 'invoicelineid'>,
  ): Promise<Invoiceline> {
    return this.trackRepository.invoicelines(id).create(invoiceline);
  }

  @patch('/tracks/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Track.Invoiceline PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invoiceline, {partial: true}),
        },
      },
    })
    invoiceline: Partial<Invoiceline>,
    @param.query.object('where', getWhereSchemaFor(Invoiceline)) where?: Where<Invoiceline>,
  ): Promise<Count> {
    return this.trackRepository.invoicelines(id).patch(invoiceline, where);
  }

  @del('/tracks/{id}/invoicelines', {
    responses: {
      '200': {
        description: 'Track.Invoiceline DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Invoiceline)) where?: Where<Invoiceline>,
  ): Promise<Count> {
    return this.trackRepository.invoicelines(id).delete(where);
  }
}
