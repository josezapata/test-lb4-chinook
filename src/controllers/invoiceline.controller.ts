import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Invoiceline} from '../models';
import {InvoicelineRepository} from '../repositories';

export class InvoicelineController {
  constructor(
    @repository(InvoicelineRepository)
    public invoicelineRepository : InvoicelineRepository,
  ) {}

  @post('/invoicelines', {
    responses: {
      '200': {
        description: 'Invoiceline model instance',
        content: {'application/json': {schema: getModelSchemaRef(Invoiceline)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invoiceline, {
            title: 'NewInvoiceline',
            exclude: ['invoicelineid'],
          }),
        },
      },
    })
    invoiceline: Omit<Invoiceline, 'invoicelineid'>,
  ): Promise<Invoiceline> {
    return this.invoicelineRepository.create(invoiceline);
  }

  @get('/invoicelines/count', {
    responses: {
      '200': {
        description: 'Invoiceline model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Invoiceline)) where?: Where<Invoiceline>,
  ): Promise<Count> {
    return this.invoicelineRepository.count(where);
  }

  @get('/invoicelines', {
    responses: {
      '200': {
        description: 'Array of Invoiceline model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Invoiceline, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Invoiceline)) filter?: Filter<Invoiceline>,
  ): Promise<Invoiceline[]> {
    return this.invoicelineRepository.find(filter);
  }

  @patch('/invoicelines', {
    responses: {
      '200': {
        description: 'Invoiceline PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invoiceline, {partial: true}),
        },
      },
    })
    invoiceline: Invoiceline,
    @param.query.object('where', getWhereSchemaFor(Invoiceline)) where?: Where<Invoiceline>,
  ): Promise<Count> {
    return this.invoicelineRepository.updateAll(invoiceline, where);
  }

  @get('/invoicelines/{id}', {
    responses: {
      '200': {
        description: 'Invoiceline model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Invoiceline, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.query.object('filter', getFilterSchemaFor(Invoiceline)) filter?: Filter<Invoiceline>
  ): Promise<Invoiceline> {
    return this.invoicelineRepository.findById(id, filter);
  }

  @patch('/invoicelines/{id}', {
    responses: {
      '204': {
        description: 'Invoiceline PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invoiceline, {partial: true}),
        },
      },
    })
    invoiceline: Invoiceline,
  ): Promise<void> {
    await this.invoicelineRepository.updateById(id, invoiceline);
  }

  @put('/invoicelines/{id}', {
    responses: {
      '204': {
        description: 'Invoiceline PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() invoiceline: Invoiceline,
  ): Promise<void> {
    await this.invoicelineRepository.replaceById(id, invoiceline);
  }

  @del('/invoicelines/{id}', {
    responses: {
      '204': {
        description: 'Invoiceline DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.invoicelineRepository.deleteById(id);
  }
}
