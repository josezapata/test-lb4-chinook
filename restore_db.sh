#!/usr/bin/env bash
docker cp db.sql test-backend_database_1:/tmp/db.sql
export PGPASSWORD='postgres'
docker exec -it test-backend_database_1 bash -c 'psql -U postgres postgres < /tmp/db.sql'
